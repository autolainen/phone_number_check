package ru.autolainen.phonenumbercheck;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static ru.autolainen.phonenumbercheck.SpringTokenCookieJar.MILLIS_IN_DAY;

public class IncomingCallReceiver extends BroadcastReceiver {

    public static final String TAG = "PhoneCheck";

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static final String COOKIE_DOMAIN = "autolainen.ru";
    //        public static final String COOKIE_DOMAIN = "10.0.0.17"; // testing
    public static final String COOKIE_PATH = "/finx";

    public static final String SERVER_URL_PREFIX = "http://" + COOKIE_DOMAIN + "/finx/";
//    public static final String SERVER_URL_PREFIX = "http://" + COOKIE_DOMAIN + ":8080/finx/";  // testing

    public static final String SERVER_URL = SERVER_URL_PREFIX + "dispatcher/ajax_json.do";
    public static final String SERVER_LOGIN_URL = SERVER_URL_PREFIX + "security_check.do";

    public static final String CHECK_NUMBER_ACTION = "ru.autolainen.phonenumbercheck.action.CHECK_NUMBER";

    public static final String PHONE_NO_EXTRA = "ru.autolainen.phonenumbercheck.extra.PHONE_NO_EXTRA";

    @Override
    public void onReceive(final Context context, Intent intent) {
        String number = null;
        if (intent.getAction().equals(TelephonyManager.ACTION_PHONE_STATE_CHANGED)) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                String stateStr = extras.getString(TelephonyManager.EXTRA_STATE);
                if (TelephonyManager.EXTRA_STATE_RINGING.equals(stateStr)) {
                    number = extras.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                }
            }
        } else if (intent.getAction().equals(CHECK_NUMBER_ACTION)) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                number = extras.getString(PHONE_NO_EXTRA, null);
            }
        }
        if (!TextUtils.isEmpty(number)) {
            new CheckPhoneAsyncTask(context).execute(number);
        }
    }

    public static boolean loginToServer(String login, String password, @NonNull SpringTokenCookieJar cookieJar) {
        RequestBody formBody = new FormBody.Builder().
                add("j_username", login).
                add("j_password", password).
                add("_spring_security_remember_me", "on").
                build();
        Request request = new Request.Builder().url(SERVER_LOGIN_URL).post(formBody).build();

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.cookieJar(cookieJar);
        OkHttpClient client = builder.build();

        Response response = null;
        boolean loginSuccessful = false;
        try {
            response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                loginSuccessful = cookieJar.hasToken();
            }
        } catch (Exception e) {
            // ignore - login failed
        } finally {
            if (response != null) {
                response.close(); // ensure the response (and underlying response body) is closed
            }
        }
        return loginSuccessful;
    }

    public static Collection<CommentVO> parsePublicComments(JSONArray publicCommentsJson) throws JSONException {
        Collection<CommentVO> comments = new ArrayList<>();
        for (int i = 0; i < publicCommentsJson.length(); i++) {
            JSONObject commentJson = publicCommentsJson.getJSONObject(i);
            long timestamp = commentJson.optLong("timestamp", -1L);
            if (timestamp > 0) {
                CommentVO commentVO = new CommentVO();
                String author = "\"" + commentJson.optString("carrier_name", "") +
                        "\"  " + commentJson.optString("dispatcher_name", "");
                commentVO.setAuthor(author);
                commentVO.setPublicComment(commentJson.optString("public_comment", ""));
                commentVO.setDateTime(timestamp);
                comments.add(commentVO);
            }
        }
        return comments;
    }
}
