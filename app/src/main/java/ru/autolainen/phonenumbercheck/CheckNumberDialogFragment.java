package ru.autolainen.phonenumbercheck;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CheckNumberDialogFragment extends DialogFragment {

    public interface CheckNumberDialogListener {
        void onDialogCheckNumberClick(String phoneNo);
    }

    // Use this instance of the interface to deliver action events
    CheckNumberDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (CheckNumberDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement CheckNumberDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.check_number_fragment, null))
                .setTitle(R.string.check_number)
                .setIcon(R.drawable.baseline_search_white_36)
                .setPositiveButton(R.string.check, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // empty listener here
                    }
                }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                CheckNumberDialogFragment.this.getDialog().cancel();
            }
        });
        return builder.create();
    }
    @Override
    public void onStart() {
        super.onStart();    //super.onStart() is where dialog.show() is actually called on the underlying dialog, so we have to do it after this point
        final AlertDialog dialog = (AlertDialog) getDialog();
        if (dialog != null) {
            Button positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText phoneNoEdit = getDialog().findViewById(R.id.check_number_edit);
                    String phoneNo = phoneNoEdit.getText().toString();
                    if (TextUtils.isEmpty(phoneNo)) {
                        phoneNoEdit.setError(getResources().getString(R.string.mandatory_field));
                    } else {
                        mListener.onDialogCheckNumberClick(phoneNo);
                        dialog.dismiss();
                    }
                }
            });
        }
    }
}
