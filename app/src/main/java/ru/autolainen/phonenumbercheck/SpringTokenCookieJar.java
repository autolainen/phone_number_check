package ru.autolainen.phonenumbercheck;

import android.content.Context;
import android.text.TextUtils;

import java.util.Collections;
import java.util.List;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.COOKIE_DOMAIN;
import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.COOKIE_PATH;

public class SpringTokenCookieJar implements CookieJar {

    private static String SPRING_SECURITY_COOKIE_KEY = "SPRING_SECURITY_REMEMBER_ME_COOKIE";

    public static final long MILLIS_IN_HOUR = 1000 * 60 * 60; // 3600000
    public static final long MILLIS_IN_DAY = MILLIS_IN_HOUR * 24;
    public static final long MILLIS_IN_30_DAYS = MILLIS_IN_DAY * 30;

    private String token = null;
    private Context mContext;

    public SpringTokenCookieJar(Context context) {
        mContext = context;
        token = SettingsProxy.getToken(context);
    }

    public boolean hasToken() {
        return !TextUtils.isEmpty(token);
    }

    @Override
    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
        if (!hasToken()) {
            if (cookies != null && !cookies.isEmpty()) {
                for (Cookie cookie : cookies) {
                    if (SPRING_SECURITY_COOKIE_KEY.equals(cookie.name())) {
                        token = cookie.value();
                        if (token != null && token.length() <= 2) { // server returns "" on failed login
                            token = null;
                        }
                        SettingsProxy.setToken(mContext, token);
                    }
                }
            }
        }
    }

    @Override
    public List<Cookie> loadForRequest(HttpUrl url) {
        List<Cookie> result;
        if (hasToken()) {
            Cookie.Builder builder =
                    new Cookie.Builder().name(SPRING_SECURITY_COOKIE_KEY).value(token)
                            .hostOnlyDomain(COOKIE_DOMAIN).path(COOKIE_PATH)
                            .expiresAt(System.currentTimeMillis() + MILLIS_IN_30_DAYS);
            Cookie cookie = builder.build();
            result = Collections.singletonList(cookie);
        } else {
            result = Collections.EMPTY_LIST;
        }
        return result;
    }
}