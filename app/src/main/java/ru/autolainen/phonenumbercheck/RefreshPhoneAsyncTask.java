package ru.autolainen.phonenumbercheck;

import android.content.Context;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import static ru.autolainen.phonenumbercheck.ContactVO.HAS_COMMENTS_STATE;
import static ru.autolainen.phonenumbercheck.ContactVO.NO_CLIENT_STATE;
import static ru.autolainen.phonenumbercheck.ContactVO.NO_COMMENTS_STATE;
import static ru.autolainen.phonenumbercheck.ContactVO.UNVERIFIED_STATE;

class RefreshPhoneAsyncTask extends BaseCheckPhoneAsyncTask {

    public RefreshPhoneAsyncTask(Context context) {
        super(context);
    }

    protected void processResponse(ContactVO contactVO) {
        DbHelper dbHelper = new DbHelper(context);
        if (contactVO.getName() == null) {
            dbHelper.updateState(contactVO.getPhone(), NO_CLIENT_STATE);
        } else {
            List<Integer> contactIds = dbHelper.getContactIdsByPhoneAndStatus(contactVO.getPhone(), UNVERIFIED_STATE);
            contactVO.setState(contactVO.getComments().isEmpty() ? NO_COMMENTS_STATE : HAS_COMMENTS_STATE);
            dbHelper.updatePhoneNumberByPhoneAndState(contactVO, UNVERIFIED_STATE);
            if (!contactVO.getComments().isEmpty()) {
                for (int contactId : contactIds) {
                    dbHelper.insertPublicCommentsByDbId(contactId, contactVO.getComments());
                }
            }
        }
    }

    @Override
    protected void processFailure(Context context, String phoneNumber) {
        EventBus.getDefault().post(R.string.check_no_exception);
    }
}
