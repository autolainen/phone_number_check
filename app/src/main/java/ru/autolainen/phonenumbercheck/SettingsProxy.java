package ru.autolainen.phonenumbercheck;

import android.content.Context;
import android.content.SharedPreferences;

public class SettingsProxy {

    public static final String PREFS_NAME = "PhoneCheckPrefs";

    public static final String LOGIN_KEY = "account_login";
    public static final String PASSWORD_KEY = "account_pwd";
    public static final String PURGE_RUN_KEY = "purge_run";
    private static String TOKEN_PREF_KEY = "token";

    public static String getToken(Context context) {
        return getPreferences(context).getString(TOKEN_PREF_KEY, "");
    }

    public static void setToken(Context context, String token) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(TOKEN_PREF_KEY, token);
        editor.commit();
    }

    public static String getLogin(Context context) {
        return getPreferences(context).getString(LOGIN_KEY, "");
    }

    public static String getPassword(Context context) {
        return getPreferences(context).getString(PASSWORD_KEY, "");
    }

    public static long getLastPurgeRun(Context context) {
        return getPreferences(context).getLong(PURGE_RUN_KEY, 0L);
    }

    public static void setLastPurgeRun(Context context, long lastPurgeRun) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putLong(PURGE_RUN_KEY, lastPurgeRun);
        editor.apply();
    }

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }
}
