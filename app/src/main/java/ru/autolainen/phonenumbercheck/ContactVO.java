package ru.autolainen.phonenumbercheck;

import java.util.ArrayList;
import java.util.List;

public class ContactVO {

    public static final int UNVERIFIED_STATE = 0;
    public static final int HAS_COMMENTS_STATE = 1;
    public static final int NO_COMMENTS_STATE = 2;
    public static final int NO_CLIENT_STATE = 3;

    private int finxId;
    private String name;
    private String surname;
    private String phone;
    private long call_time;
    private int state;
    private List<CommentVO> comments = null;

    public int getFinxId() {
        return finxId;
    }

    public void setFinxId(int finxId) {
        this.finxId = finxId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getCallTime() {
        return call_time;
    }

    public void setCallTime(long call_time) {
        this.call_time = call_time;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public List<CommentVO> getComments() {
        if (comments == null) {
            comments = new ArrayList<>();
        }
        return comments;
    }
}
