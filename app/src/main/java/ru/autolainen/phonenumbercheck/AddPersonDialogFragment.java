package ru.autolainen.phonenumbercheck;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static ru.autolainen.phonenumbercheck.ContactListActivity.PHONE_NO_EXTRA_KEY;

public class AddPersonDialogFragment extends DialogFragment {

    public interface AddPersonDialogListener {
        void onAddPersonClick(ContactVO contactVO);
    }

    // Use this instance of the interface to deliver action events
    AddPersonDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (AddPersonDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement AddPersonDialogListener");
        }
        if (TextUtils.isEmpty(getArguments().getString(PHONE_NO_EXTRA_KEY))) {
            throw new IllegalArgumentException("Phone number must be supplied");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.add_person_fragment, null))
                .setTitle(R.string.add_client)
                .setIcon(R.drawable.baseline_person_add_white_24)
                .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // empty listener here
                    }
                }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                AddPersonDialogFragment.this.getDialog().cancel();
            }
        });
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();    //super.onStart() is where dialog.show() is actually called on the underlying dialog, so we have to do it after this point
        final AlertDialog dialog = (AlertDialog) getDialog();
        if (dialog != null) {

            final String phoneNo = getArguments().getString(PHONE_NO_EXTRA_KEY);
            TextView personPhone = getDialog().findViewById(R.id.person_phone);
            String number = getResources().getString(R.string.number);
            personPhone.setText(number + ": " + phoneNo);

            Button positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText personNameEdit = getDialog().findViewById(R.id.person_name);
                    String personName = personNameEdit.getText().toString();
                    if (TextUtils.isEmpty(personName)) {
                        personNameEdit.setError(getResources().getString(R.string.mandatory_field));
                    } else {
                        EditText personSurnameEdit = getDialog().findViewById(R.id.person_surname);
                        ContactVO contactVO = new ContactVO();
                        contactVO.setPhone(phoneNo);
                        contactVO.setName(personName);
                        contactVO.setSurname(personSurnameEdit.getText().toString());
                        mListener.onAddPersonClick(contactVO);
                        dialog.dismiss();
                    }
                }
            });
        }
    }
}
