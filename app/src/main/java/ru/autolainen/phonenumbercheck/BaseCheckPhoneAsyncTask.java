package ru.autolainen.phonenumbercheck;

import android.content.Context;
import android.os.AsyncTask;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.JSON;
import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.SERVER_URL;
import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.loginToServer;
import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.parsePublicComments;
import static ru.autolainen.phonenumbercheck.SpringTokenCookieJar.MILLIS_IN_DAY;

abstract class BaseCheckPhoneAsyncTask extends AsyncTask<String, Object, Object> {

    protected Context context;

    public BaseCheckPhoneAsyncTask(Context context) {
        this.context = context.getApplicationContext();
    }

    @Override
    protected Object doInBackground(String... arguments) {
        // purge
        if (System.currentTimeMillis() - SettingsProxy.getLastPurgeRun(context) > MILLIS_IN_DAY) {
            DbHelper dbHelper = new DbHelper(context);
            dbHelper.purge();
            SettingsProxy.setLastPurgeRun(context, System.currentTimeMillis());
        }

        makeServerCall(context, arguments[0]);
        return null;
    }

    public void makeServerCall(final Context context, final String phoneNumber) {
        try {
            SpringTokenCookieJar cookieJar = new SpringTokenCookieJar(context);
            if (!cookieJar.hasToken()) {
                loginToServer(SettingsProxy.getLogin(context), SettingsProxy.getPassword(context), cookieJar);
            }
            if (cookieJar.hasToken()) { // already logged in
                JSONObject requestJson = new JSONObject();
                requestJson.put("method", "get_public_comments_by_phone");
                requestJson.put("client_phone", phoneNumber);

                RequestBody body = RequestBody.create(JSON, requestJson.toString());
                Request request = new Request.Builder()
                        .url(SERVER_URL).post(body).build();

                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.cookieJar(cookieJar);
                OkHttpClient client = builder.build();

                Response response = null;
                ResponseBody responseBody = null;
                try {
                    response = client.newCall(request).execute();
                    if (response.isSuccessful()) {
                        try {
                            responseBody = response.body();
                            String responseStr = responseBody.string();
                            processResponse(context, responseStr, phoneNumber);
                        } catch (Exception e) {
                            SettingsProxy.setToken(context, "");
                            processFailure(context, phoneNumber);
                        }
                    } else {
                        SettingsProxy.setToken(context, "");
                        processFailure(context, phoneNumber);
                    }
                } catch (Exception e) {
                    processFailure(context, phoneNumber);
                } finally {
                    if (responseBody != null) {
                        responseBody.close();
                    }
                    if (response != null) {
                        response.close(); // ensure the response (and underlying response body) is closed
                    }
                }
            } else {
                processFailure(context, phoneNumber); // not logged in
            }
        } catch (Exception e) {
            processFailure(context, phoneNumber);
        }
        EventBus.getDefault().post(new RefreshEvent());
    }

    private void processResponse(Context context, String responseStr, String phoneNumber) throws JSONException {
        JSONObject responseJson = new JSONObject(responseStr);
        ContactVO contactVO = new ContactVO();
        contactVO.setPhone(phoneNumber);
        JSONObject clientJson = responseJson.optJSONObject("client");
        if (clientJson != null) {
            contactVO.setFinxId(clientJson.getInt("id"));
            contactVO.setName(clientJson.optString("name", ""));
            contactVO.setSurname(clientJson.optString("surname", ""));
        }
        JSONArray publicCommentsJson = responseJson.optJSONArray("public_comments");
        if (publicCommentsJson != null && publicCommentsJson.length() > 0) {
            contactVO.getComments().addAll(parsePublicComments(publicCommentsJson));
        }
        processResponse(contactVO);
    }

    abstract protected void processResponse(ContactVO contactVO);

    abstract protected void processFailure(Context context, String phoneNumber);

}
