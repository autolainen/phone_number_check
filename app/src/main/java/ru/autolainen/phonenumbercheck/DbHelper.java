package ru.autolainen.phonenumbercheck;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Environment;
import android.provider.BaseColumns;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static ru.autolainen.phonenumbercheck.ContactVO.NO_COMMENTS_STATE;
import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.TAG;
import static ru.autolainen.phonenumbercheck.SpringTokenCookieJar.MILLIS_IN_DAY;

public class DbHelper extends SQLiteOpenHelper {

    private static final String PHONE_TABLE_NAME = "phone_number";

    private static final String COLUMN_NAME_FINX_ID = "finx_id";
    private static final String COLUMN_NAME_PHONE_NUMBER = "phone_no";
    private static final String COLUMN_NAME_CONTACT_NAME = "contact_name";
    private static final String COLUMN_NAME_CALL_TIME = "call_time";
    private static final String COLUMN_NAME_STATE = "state";

    private static final String COMMENTS_TABLE_NAME = "contact_comments";

    private static final String COLUMN_NAME_PHONE_ID = "phone_id";
    private static final String COLUMN_NAME_COMMENT_TIME = "comment_time";
    private static final String COLUMN_NAME_COMMENT_AUTHOR = "author";
    private static final String COLUMN_NAME_COMMENT = "comment";

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 4;
    static final String DATABASE_NAME = "PhoneComment.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";

    private final String SQL_CREATE_PHONE_NUMBER_TABLE =
            "CREATE TABLE " + PHONE_TABLE_NAME + " (" +
                    BaseColumns._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
                    COLUMN_NAME_FINX_ID + INTEGER_TYPE + COMMA_SEP +
                    COLUMN_NAME_PHONE_NUMBER + TEXT_TYPE + COMMA_SEP +
                    COLUMN_NAME_CONTACT_NAME + TEXT_TYPE + COMMA_SEP +
                    COLUMN_NAME_CALL_TIME + INTEGER_TYPE + COMMA_SEP +
                    COLUMN_NAME_STATE + INTEGER_TYPE +
                    " )";

    private final String SQL_CREATE_CONTACT_COMMENTS_TABLE =
            "CREATE TABLE " + COMMENTS_TABLE_NAME + " (" +
                    BaseColumns._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
                    COLUMN_NAME_PHONE_ID + INTEGER_TYPE + COMMA_SEP +
                    COLUMN_NAME_COMMENT_TIME + INTEGER_TYPE + COMMA_SEP +
                    COLUMN_NAME_COMMENT_AUTHOR + TEXT_TYPE + COMMA_SEP +
                    COLUMN_NAME_COMMENT + TEXT_TYPE + COMMA_SEP +
                    "FOREIGN KEY(" + COLUMN_NAME_PHONE_ID + ") REFERENCES " +
                    PHONE_TABLE_NAME + "(" + BaseColumns._ID + ") ON DELETE CASCADE" +
                    " )";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PHONE_NUMBER_TABLE);
        db.execSQL(SQL_CREATE_CONTACT_COMMENTS_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL("DROP TABLE IF EXISTS " + COMMENTS_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PHONE_TABLE_NAME);
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.execSQL("PRAGMA foreign_keys = ON;");
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public List<ContactVO> getContacts() {
        SQLiteDatabase db = getReadableDatabase();

        List<ContactVO> result = new ArrayList<>();
        Cursor allRowsCursor = null;
        try {
            String table = PHONE_TABLE_NAME + " as ph" +
                    " left join " + COMMENTS_TABLE_NAME + " as cm on ph._id = cm." +
                    COLUMN_NAME_PHONE_ID;
            String columns[] = {
                    "ph." + BaseColumns._ID + " as " + BaseColumns._ID,
                    "ph." + COLUMN_NAME_FINX_ID + " as " + COLUMN_NAME_FINX_ID,
                    "ph." + COLUMN_NAME_PHONE_NUMBER + " as " + COLUMN_NAME_PHONE_NUMBER,
                    "ph." + COLUMN_NAME_CONTACT_NAME + " as " + COLUMN_NAME_CONTACT_NAME,
                    "ph." + COLUMN_NAME_CALL_TIME + " as " + COLUMN_NAME_CALL_TIME,
                    "ph." + COLUMN_NAME_STATE + " as " + COLUMN_NAME_STATE,
                    "cm." + COLUMN_NAME_PHONE_ID + " as " + COLUMN_NAME_PHONE_ID,
                    "cm." + COLUMN_NAME_COMMENT_TIME + " as " + COLUMN_NAME_COMMENT_TIME,
                    "cm." + COLUMN_NAME_COMMENT_AUTHOR + " as " + COLUMN_NAME_COMMENT_AUTHOR,
                    "cm." + COLUMN_NAME_COMMENT + " as " + COLUMN_NAME_COMMENT
            };

            allRowsCursor = db.query(table, columns, null, null, null, null, COLUMN_NAME_CALL_TIME + " DESC" +
                    "," + COLUMN_NAME_COMMENT_TIME + " DESC");
            int phoneIdColumn = allRowsCursor.getColumnIndex(BaseColumns._ID);
            int finxIdColumn = allRowsCursor.getColumnIndex(COLUMN_NAME_FINX_ID);
            int phoneColumn = allRowsCursor.getColumnIndex(COLUMN_NAME_PHONE_NUMBER);
            int contactNameColumn = allRowsCursor.getColumnIndex(COLUMN_NAME_CONTACT_NAME);
            int callTimeColumn = allRowsCursor.getColumnIndex(COLUMN_NAME_CALL_TIME);
            int stateColumn = allRowsCursor.getColumnIndex(COLUMN_NAME_STATE);
            int commentPhoneIdColumn = allRowsCursor.getColumnIndex(COLUMN_NAME_PHONE_ID);
            int commentTimeColumn = allRowsCursor.getColumnIndex(COLUMN_NAME_COMMENT_TIME);
            int commentAuthorColumn = allRowsCursor.getColumnIndex(COLUMN_NAME_COMMENT_AUTHOR);
            int commentColumn = allRowsCursor.getColumnIndex(COLUMN_NAME_COMMENT);

            long prevPhoneId = -1;
            ContactVO contactVO = null;

            while (allRowsCursor.moveToNext()) {
                long phoneId = allRowsCursor.getLong(phoneIdColumn);
                if (prevPhoneId != phoneId) {
                    contactVO = new ContactVO();
                    contactVO.setFinxId(allRowsCursor.getInt(finxIdColumn));
                    contactVO.setPhone(allRowsCursor.getString(phoneColumn));
                    contactVO.setName(allRowsCursor.getString(contactNameColumn));
                    contactVO.setCallTime(allRowsCursor.getLong(callTimeColumn));
                    contactVO.setState(allRowsCursor.getInt(stateColumn));
                    result.add(contactVO);
                    prevPhoneId = phoneId;
                }
                if (allRowsCursor.getLong(commentPhoneIdColumn) > 0) {
                    CommentVO commentVO = new CommentVO();
                    commentVO.setAuthor(allRowsCursor.getString(commentAuthorColumn));
                    commentVO.setDateTime(allRowsCursor.getLong(commentTimeColumn));
                    commentVO.setPublicComment(allRowsCursor.getString(commentColumn));
                    contactVO.getComments().add(commentVO);
                }
            }
        } finally {
            if (allRowsCursor != null) {
                allRowsCursor.close();
            }
        }
        return result;
    }

    public long countPhoneNumberInStatus(String number, int state) {
        SQLiteDatabase db = getReadableDatabase();
        return DatabaseUtils.queryNumEntries(db, PHONE_TABLE_NAME,
                COLUMN_NAME_PHONE_NUMBER + "=? AND " +
                        COLUMN_NAME_STATE + "=?",
                new String[]{number, String.valueOf(state)});
    }

    public long updatePhoneNumberByPhoneAndState(ContactVO contactVO, int state) {
        long result = -1;
        if (!TextUtils.isEmpty(contactVO.getPhone())) {
            SQLiteDatabase db = getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME_CONTACT_NAME, contactVO.getName() + " " + contactVO.getSurname());
            values.put(COLUMN_NAME_STATE, contactVO.getState());
            values.put(COLUMN_NAME_FINX_ID, contactVO.getFinxId());

            result = db.update(PHONE_TABLE_NAME, values,
                    COLUMN_NAME_PHONE_NUMBER + "=? and " +
                            COLUMN_NAME_STATE + "=" + state,
                    new String[]{contactVO.getPhone()});
        }
        return result;
    }

    public long updatePhoneNumberByPhone(ContactVO contactVO) {
        long result = -1;
        if (!TextUtils.isEmpty(contactVO.getPhone())) {
            SQLiteDatabase db = getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME_CONTACT_NAME, contactVO.getName() + " " + contactVO.getSurname());
            values.put(COLUMN_NAME_STATE, NO_COMMENTS_STATE);
            values.put(COLUMN_NAME_FINX_ID, contactVO.getFinxId());

            result = db.update(PHONE_TABLE_NAME, values,
                    COLUMN_NAME_PHONE_NUMBER + "=?", new String[]{contactVO.getPhone()});
        }
        return result;
    }

    public long insertPhoneNumber(int finxId, String number, String name, long callTime,
                                  int state, Collection<CommentVO> comments) {
        long result = -1;
        if (!TextUtils.isEmpty(number)) {

            SQLiteDatabase db = getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME_FINX_ID, finxId);
            values.put(COLUMN_NAME_PHONE_NUMBER, number);
            values.put(COLUMN_NAME_CONTACT_NAME, name);
            values.put(COLUMN_NAME_CALL_TIME, callTime);
            values.put(COLUMN_NAME_STATE, state);

            db.beginTransaction();
            try {
                long phoneDbId = db.insert(PHONE_TABLE_NAME, null, values);
                insertPublicComments(db, phoneDbId, comments);
                db.setTransactionSuccessful();
                result = phoneDbId;
            } finally {
                db.endTransaction();
            }
        }
        return result;
    }

    public void insertPublicComments(long finxId, Collection<CommentVO> comments) {
        SQLiteDatabase db = getWritableDatabase();

        Cursor phoneDbIdCursor = null;
        try {
            phoneDbIdCursor = db.query(PHONE_TABLE_NAME, new String[]{BaseColumns._ID},
                    COLUMN_NAME_FINX_ID + "=" + finxId,
                    null, null, null, null, null);
            int phoneDbIdIndex = phoneDbIdCursor.getColumnIndex(BaseColumns._ID);
            while (phoneDbIdCursor.moveToNext()) {
                int phoneDbId = phoneDbIdCursor.getInt(phoneDbIdIndex);
                insertPublicComments(db, phoneDbId, comments);
            }
        } finally {
            if (phoneDbIdCursor != null) {
                phoneDbIdCursor.close();
            }
        }
    }

    public void insertPublicCommentsByDbId(long phoneDbId, Collection<CommentVO> comments) {
        insertPublicComments(getWritableDatabase(), phoneDbId, comments);
    }

    private void insertPublicComments(SQLiteDatabase db, long phoneDbId, Collection<CommentVO> comments) {
        if (comments != null && !comments.isEmpty()) {
            for (CommentVO commentVO : comments) {
                ContentValues commentValues = new ContentValues();
                commentValues.put(COLUMN_NAME_PHONE_ID, phoneDbId);
                commentValues.put(COLUMN_NAME_COMMENT_TIME, commentVO.getDateTime());
                commentValues.put(COLUMN_NAME_COMMENT_AUTHOR, commentVO.getAuthor());
                commentValues.put(COLUMN_NAME_COMMENT, commentVO.getPublicComment());
                db.insert(COMMENTS_TABLE_NAME, null, commentValues);
            }
        }
    }

    public void updateState(long finxId, int state) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME_STATE, state);
            db.update(PHONE_TABLE_NAME, values,
                    COLUMN_NAME_FINX_ID + "=" + finxId, null);
        } catch (Exception e) {
            // ignore
        }
    }

    public void updateState(String phone, int state) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME_STATE, state);
            db.update(PHONE_TABLE_NAME, values,
                    COLUMN_NAME_PHONE_NUMBER + "=?", new String[]{phone});
        } catch (Exception e) {
            // ignore
        }
    }

    public void purge() {
        try {
            SQLiteDatabase db = getWritableDatabase();
            db.delete(PHONE_TABLE_NAME,
                    COLUMN_NAME_CALL_TIME + "<" + (System.currentTimeMillis() - MILLIS_IN_DAY),
                    null);
        } catch (Exception e) {
            // ignore
        }
    }

    public void updateCallTime(String number, long timeMillis) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME_CALL_TIME, timeMillis);
            db.update(PHONE_TABLE_NAME, values,
                    COLUMN_NAME_PHONE_NUMBER + "=?", new String[]{number});
        } catch (Exception e) {
            // ignore
        }
    }

    public void deletePublicComments(int finxId) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            db.delete(COMMENTS_TABLE_NAME, COLUMN_NAME_PHONE_ID + " in (SELECT " + BaseColumns._ID
                    + " FROM " + PHONE_TABLE_NAME + " WHERE " + COLUMN_NAME_FINX_ID + "=" + finxId + ")", null);
        } catch (Exception e) {
            // ignore
        }
    }

    public ContactVO getLatestCallEntry(int finxId) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = null;

        ContactVO result;
        try {
            cursor = db.query(PHONE_TABLE_NAME, new String[]{COLUMN_NAME_PHONE_NUMBER, COLUMN_NAME_CONTACT_NAME},
            COLUMN_NAME_FINX_ID + "=" + finxId, null, null, null,
            COLUMN_NAME_CALL_TIME + " DESC", "1");

            result = null;
            while (cursor.moveToNext()) {
                int phoneColumn = cursor.getColumnIndex(COLUMN_NAME_PHONE_NUMBER);
                int contactNameColumn = cursor.getColumnIndex(COLUMN_NAME_CONTACT_NAME);
                result = new ContactVO();
                result.setFinxId(finxId);
                result.setPhone(cursor.getString(phoneColumn));
                result.setName(cursor.getString(contactNameColumn));
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return result;
    }

    public List<Integer> getContactIdsByPhoneAndStatus(String phone, int state) {
        SQLiteDatabase db = getReadableDatabase();
        List<Integer> result = new ArrayList<>();
        Cursor cursor = null;

        try {
            cursor = db.query (PHONE_TABLE_NAME, new String[]{BaseColumns._ID},
                    COLUMN_NAME_PHONE_NUMBER + "=? and " + COLUMN_NAME_STATE + "=" + state,
                    new String[] {phone}, null, null, null, null);
            int idColumn = cursor.getColumnIndex(BaseColumns._ID);
            while (cursor.moveToNext()) {
                result.add(cursor.getInt(idColumn));
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return result;
    }

    public void updateFinxId(int finxId, int newFinxId) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME_FINX_ID, newFinxId);
            db.update(PHONE_TABLE_NAME, values,
                    COLUMN_NAME_FINX_ID + "=" + finxId, null);
        } catch (Exception e) {
            // ignore
        }
    }


    public void writeToSD(Activity context) { // for testing only
        try {
            File sd = Environment.getExternalStorageDirectory();
            String DB_PATH;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                DB_PATH = context.getFilesDir().getAbsolutePath().replace("files", "databases") + File.separator;
            }
            else {
                DB_PATH = context.getFilesDir().getPath() + context.getPackageName() + "/databases/";
            }
            if (sd.canWrite()) {
                String currentDBPath = DATABASE_NAME;
                String backupDBPath = DATABASE_NAME;
                File currentDB = new File(DB_PATH, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            } else {
                ActivityCompat.requestPermissions(context,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}