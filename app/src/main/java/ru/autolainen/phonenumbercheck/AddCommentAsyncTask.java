package ru.autolainen.phonenumbercheck;

import android.content.Context;
import android.os.AsyncTask;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.JSON;
import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.SERVER_URL;
import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.loginToServer;
import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.parsePublicComments;

class AddCommentAsyncTask extends AsyncTask<Object, Object, Object> {

    private Context context;

    public AddCommentAsyncTask(Context context) {
        this.context = context.getApplicationContext();
    }

    @Override
    protected Object doInBackground(Object... arguments) {
        makeServerCall(context, (Integer) arguments[0], (String) arguments[1]);
        return null;
    }

    private void makeServerCall(final Context context, int finxId, final String personComment) {
        boolean successful = true;
        try {
            SpringTokenCookieJar cookieJar = new SpringTokenCookieJar(context);
            if (!cookieJar.hasToken()) {
                loginToServer(SettingsProxy.getLogin(context), SettingsProxy.getPassword(context), cookieJar);
            }
            if (cookieJar.hasToken()) { // already logged in
                JSONObject requestJson = new JSONObject();
                requestJson.put("method", "add_public_comment");
                requestJson.put("client_id", finxId);
                requestJson.put("public_comment", personComment);

                RequestBody body = RequestBody.create(JSON, requestJson.toString());
                Request request = new Request.Builder()
                        .url(SERVER_URL).post(body).build();

                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.cookieJar(cookieJar);
                OkHttpClient client = builder.build();

                Response response = null;
                ResponseBody responseBody = null;
                try {
                    response = client.newCall(request).execute();
                    if (response.isSuccessful()) {
                        try {
                            responseBody = response.body();
                            String responseStr = responseBody.string();
                            processResponse(context, responseStr, finxId);
                        } catch (Exception e) {
                            SettingsProxy.setToken(context, "");
                            successful = false;
                        }
                    } else {
                        SettingsProxy.setToken(context, "");
                        successful = false;
                    }
                } finally {
                    if (responseBody != null) {
                        responseBody.close();
                    }
                    if (response != null) {
                        response.close(); // ensure the response (and underlying response body) is closed
                    }
                }
            } else {
                successful = false;
            }
        } catch (Exception e) {
            successful = false;
        }
        if (successful) {
            EventBus.getDefault().post(new RefreshEvent());
        } else {
            EventBus.getDefault().post(R.string.add_comment_exception);
        }
    }

    private void processResponse(Context context, String responseStr, int finxId) throws JSONException {
        JSONObject responseJson = new JSONObject(responseStr);
        int newFinxId = responseJson.getInt("client_id");
        DbHelper dbHelper = new DbHelper(context);
        if (newFinxId != finxId) {
            dbHelper.updateFinxId(finxId, newFinxId);
            finxId = newFinxId;
        }

        JSONArray publicCommentsJson = responseJson.optJSONArray("public_comments");
        if (publicCommentsJson != null && publicCommentsJson.length() > 0) {
            Collection<CommentVO> comments = parsePublicComments(publicCommentsJson);

            dbHelper.deletePublicComments(finxId);
            dbHelper.insertPublicComments(finxId, comments);
            dbHelper.updateState(finxId, comments.size() > 0 ? ContactVO.HAS_COMMENTS_STATE : ContactVO.NO_COMMENTS_STATE);
        }
    }
}
