package ru.autolainen.phonenumbercheck;

import android.Manifest;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.CHECK_NUMBER_ACTION;
import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.PHONE_NO_EXTRA;

public class ContactListActivity extends AppCompatActivity
        implements CheckNumberDialogFragment.CheckNumberDialogListener,
        AddPersonDialogFragment.AddPersonDialogListener,
        AddCommentDialogFragment.AddCommentDialogListener {

    private static final String CALL_DATE_TIME_FORMAT = "dd MMM   HH:mm";
    private static final String COMMENT_DATE_FORMAT = "dd MMM yyyy";
    private ContactListAdapter contactListAdapter;

    public static final String PHONE_NO_EXTRA_KEY = "phoneNo";
    public static final String FINX_ID_EXTRA_KEY = "finxId";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.contact_list_layout);

        ListView list = findViewById(R.id.contact_list);
        contactListAdapter = new ContactListAdapter(this);
        list.setAdapter(contactListAdapter);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.check_number_item:
                DialogFragment dialog = new CheckNumberDialogFragment();
                dialog.show(getFragmentManager(), "CheckNumberDialogFragment");
                return true;
            case R.id.settings_item:
                startActivity(new Intent(this, SettingsActivity.class));
//                new DbHelper(this).writeToSD(this); // for testing only
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        refreshData();
    }

    private void refreshData() {
        if (contactListAdapter != null) {
            contactListAdapter.loadContent();
            contactListAdapter.notifyDataSetChanged();
        }
    }

    // EventBus functionality
    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onToastEvent(Integer stringResource) {
        Toast.makeText(this, stringResource, Toast.LENGTH_SHORT).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRefreshEvent(RefreshEvent refreshEvent) {
        refreshData();
    }

    @Override
    public void onDialogCheckNumberClick(String phoneNo) {
        Intent showGroupsListIntent = new Intent(this, IncomingCallReceiver.class);
        showGroupsListIntent.setAction(CHECK_NUMBER_ACTION);
        showGroupsListIntent.putExtra(PHONE_NO_EXTRA, phoneNo);
        sendBroadcast(showGroupsListIntent);
    }

    @Override
    public void onAddPersonClick(ContactVO contactVO) {
        new AddPersonAsyncTask(this).execute(contactVO);
    }

    @Override
    public void onAddCommentClick(int finxId, String personComment) {
        new AddCommentAsyncTask(this).execute(finxId, personComment);
    }

    private class ContactListAdapter extends BaseAdapter {

        private Context context;
        private List<ContactVO> listContent;
        private DateFormat callDateTimeFormatter = new SimpleDateFormat(CALL_DATE_TIME_FORMAT);
        private DateFormat commentDateFormatter = new SimpleDateFormat(COMMENT_DATE_FORMAT);

        public ContactListAdapter(@NonNull Context context) {
            this.context = context;
            loadContent();
        }

        public void loadContent() {
            DbHelper dbHelper = new DbHelper(context);
            listContent = new ArrayList<>(dbHelper.getContacts());
        }

        @Override
        public int getCount() {
            return listContent == null ? 0 : listContent.size();
        }

        @Override
        public Object getItem(int position) {
            return listContent == null ? null : listContent.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View result = convertView;
            if (convertView == null) {
                result = getLayoutInflater().inflate(R.layout.contact_item_layout, parent, false);
            }
            View stateIndicator = result.findViewById(R.id.state_indicator);
            TextView contactName = result.findViewById(R.id.contact_name);
            TextView phoneNumber = result.findViewById(R.id.phone_number);
            TextView dateTime = result.findViewById(R.id.date_time);
            final ImageButton retryBtn = result.findViewById(R.id.retry_btn);
            ImageButton personAddBtn = result.findViewById(R.id.person_add_btn);
            ImageButton commentAddBtn = result.findViewById(R.id.comment_add_btn);
            ViewGroup commentsContainer = result.findViewById(R.id.comments_container);

            retryBtn.setVisibility(View.GONE);
            retryBtn.setEnabled(true);
            personAddBtn.setVisibility(View.GONE);
            commentAddBtn.setVisibility(View.GONE);
            // in some cases position may be out of bounds
            if (listContent != null && listContent.size() > 0) {
                position = Math.min(Math.max(0, position), listContent.size());
                final ContactVO contactVO = listContent.get(position);

                phoneNumber.setText(contactVO.getPhone());
                commentsContainer.removeAllViews();
                dateTime.setText(callDateTimeFormatter.format(contactVO.getCallTime()));
                if (contactVO.getState() == ContactVO.UNVERIFIED_STATE) {
                    contactName.setVisibility(View.GONE);
                    getLayoutInflater().inflate(R.layout.unverified_phone_msg, commentsContainer);
                    stateIndicator.setBackgroundColor(getResources().getColor(R.color.yellowIndicator));
                    retryBtn.setVisibility(View.VISIBLE);
                } else if (contactVO.getState() == ContactVO.NO_COMMENTS_STATE) {
                    contactName.setVisibility(View.VISIBLE);
                    contactName.setText(contactVO.getName());
                    stateIndicator.setBackgroundColor(getResources().getColor(R.color.greenIndicator));
                    commentAddBtn.setVisibility(View.VISIBLE);
                } else if (contactVO.getState() == ContactVO.NO_CLIENT_STATE) {
                    contactName.setVisibility(View.GONE);
                    getLayoutInflater().inflate(R.layout.no_client_msg, commentsContainer);
                    stateIndicator.setBackgroundColor(getResources().getColor(R.color.blackIndicator));
                    personAddBtn.setVisibility(View.VISIBLE);
                } else if (contactVO.getState() == ContactVO.HAS_COMMENTS_STATE) {
                    contactName.setVisibility(View.VISIBLE);
                    contactName.setText(contactVO.getName());
                    for (CommentVO commentVO : contactVO.getComments()) {
                        View commentLayout = getLayoutInflater().inflate(R.layout.comment_layout, commentsContainer, false);
                        TextView commentDatetime = commentLayout.findViewById(R.id.comment_datetime);
                        TextView commentAuthor = commentLayout.findViewById(R.id.comment_author);
                        TextView commentText = commentLayout.findViewById(R.id.comment_text);

                        commentDatetime.setText(commentDateFormatter.format(commentVO.getDateTime()));
                        commentAuthor.setText(String.valueOf(commentVO.getAuthor()));
                        commentText.setText(String.valueOf(commentVO.getPublicComment()));
                        commentsContainer.addView(commentLayout);
                    }
                    stateIndicator.setBackgroundColor(getResources().getColor(R.color.redIndicator));
                    commentAddBtn.setVisibility(View.VISIBLE);
                }
                if (personAddBtn.getVisibility() == View.VISIBLE) {
                    personAddBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DialogFragment dialog = new AddPersonDialogFragment();
                            Bundle args = new Bundle();
                            args.putString(PHONE_NO_EXTRA_KEY, contactVO.getPhone());
                            dialog.setArguments(args);
                            dialog.show(getFragmentManager(), "AddPersonDialogFragment");
                        }
                    });
                }
                if (commentAddBtn.getVisibility() == View.VISIBLE) {
                    commentAddBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DialogFragment dialog = new AddCommentDialogFragment();
                            Bundle args = new Bundle();
                            args.putInt(FINX_ID_EXTRA_KEY, contactVO.getFinxId());
                            dialog.setArguments(args);
                            dialog.show(getFragmentManager(), "AddCommentDialogFragment");
                        }
                    });
                }
                if (retryBtn.getVisibility() == View.VISIBLE) {
                    retryBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            retryBtn.setEnabled(false);
                            new RefreshPhoneAsyncTask(ContactListActivity.this).execute(contactVO.getPhone());
                        }
                    });
                }
            }
            return result;
        }
    }
}
