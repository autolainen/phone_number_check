package ru.autolainen.phonenumbercheck;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        setTitle(getResources().getString(R.string.settings));

        // Display the fragment as the main content.
        if (savedInstanceState == null) {

            SettingsFragment settingsFragment = new SettingsFragment();

            Bundle args = new Bundle();
            settingsFragment.setArguments(args);

            getFragmentManager().beginTransaction()
                    .replace(R.id.settings_content, settingsFragment).commit();
        }
    }
}

