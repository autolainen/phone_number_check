package ru.autolainen.phonenumbercheck;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import static ru.autolainen.phonenumbercheck.SettingsProxy.PASSWORD_KEY;
import static ru.autolainen.phonenumbercheck.SettingsProxy.PREFS_NAME;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PreferenceManager manager = getPreferenceManager();
        manager.setSharedPreferencesName(PREFS_NAME);

        addPreferencesFromResource(R.xml.preferences); // Load the preferences from an XML resource

        initSummary(getPreferenceScreen());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment, container, false);
        final View tryLogin = view.findViewById(R.id.tryLogin);
        final Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                tryLogin.setEnabled(true);
                return false;
            }
        });
        tryLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryLogin.setEnabled(false);
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final Context context = container.getContext();
                        SettingsProxy.setToken(context, null);
                        SpringTokenCookieJar cookieJar = new SpringTokenCookieJar(context);

                        final boolean isSuccessful = IncomingCallReceiver.loginToServer(SettingsProxy.getLogin(context),
                                SettingsProxy.getPassword(context), cookieJar);
                        handler.sendEmptyMessage(0);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, isSuccessful ? "Вход выполнен"  : "Не удалось подключиться", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
                thread.start();
            }
        });
        return view;
    }

    private void initSummary(Preference p) {
        if (p instanceof PreferenceGroup) {
            PreferenceGroup pGrp = (PreferenceGroup) p;
            for (int i = 0; i < pGrp.getPreferenceCount(); i++) {
                initSummary(pGrp.getPreference(i));
            }
        } else {
            updatePrefSummary(p);
        }
    }

    private void updatePrefSummary(Preference p) {
        if (p != null) { // NPE crash fix
            if (p instanceof ListPreference) {
                ListPreference listPref = (ListPreference) p;
                p.setSummary(listPref.getEntry());
            }
            if (p instanceof EditTextPreference && !PASSWORD_KEY.equals(p.getKey())) {
                EditTextPreference editTextPref = (EditTextPreference) p;
                p.setSummary(editTextPref.getText());
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference pref = findPreference(key);
        updatePrefSummary(pref);
        if (SettingsProxy.LOGIN_KEY.equals(key) ||
                SettingsProxy.PASSWORD_KEY.equals(key)) {
            SettingsProxy.setToken(getActivity(), "");
        }
    }
}
