package ru.autolainen.phonenumbercheck;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static ru.autolainen.phonenumbercheck.ContactListActivity.FINX_ID_EXTRA_KEY;

public class AddCommentDialogFragment extends DialogFragment {

    public interface AddCommentDialogListener {
        void onAddCommentClick(int finxId, String personComment);
    }

    // Use this instance of the interface to deliver action events
    AddCommentDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (AddCommentDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement AddCommentDialogListener");
        }
        if (getArguments().getInt(FINX_ID_EXTRA_KEY, -1) <= 0) {
            throw new IllegalArgumentException("Finx id must be supplied");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.add_comment_fragment, null))
                .setTitle(R.string.add_comment)
                .setIcon(R.drawable.baseline_announcement_white_24)
                .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // empty listener here
                    }
                }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                AddCommentDialogFragment.this.getDialog().cancel();
            }
        });
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();    // super.onStart() is where dialog.show() is actually called on the underlying dialog, so we have to do it after this point
        final AlertDialog dialog = (AlertDialog) getDialog();
        final int finxId = getArguments().getInt(FINX_ID_EXTRA_KEY, -1);
        Activity activity = getActivity();
        if (dialog != null && finxId > 0 && activity != null) {
            DbHelper dbHelper = new DbHelper(activity);
            ContactVO callEntry = dbHelper.getLatestCallEntry(finxId);
            if (callEntry == null) {
                dialog.dismiss();
            } else {
                TextView personPhone = getDialog().findViewById(R.id.person_phone);
                TextView personName = getDialog().findViewById(R.id.person_name);
                personPhone.setText(callEntry.getPhone());
                personName.setText(callEntry.getName());

                Button positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText personCommentEdit = getDialog().findViewById(R.id.person_comment);
                        String personComment = personCommentEdit.getText().toString();
                        if (TextUtils.isEmpty(personComment)) {
                            personCommentEdit.setError(getResources().getString(R.string.mandatory_field));
                        } else {
                            mListener.onAddCommentClick(finxId, personComment);
                            dialog.dismiss();
                        }
                    }
                });
            }
        }
    }
}
