package ru.autolainen.phonenumbercheck;

import android.content.Context;
import android.os.AsyncTask;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.JSON;
import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.SERVER_URL;
import static ru.autolainen.phonenumbercheck.IncomingCallReceiver.loginToServer;

class AddPersonAsyncTask extends AsyncTask<ContactVO, Object, Object>{

    private Context context;

    public AddPersonAsyncTask(Context context) {
        this.context = context.getApplicationContext();
    }

    @Override
    protected Object doInBackground(ContactVO... contactVOs) {
        makeServerCall(context, contactVOs[0]);
        return null;
    }

    private void makeServerCall(final Context context, final ContactVO contactVO) {
        boolean successful = true;
        try {
            SpringTokenCookieJar cookieJar = new SpringTokenCookieJar(context);
            if (!cookieJar.hasToken()) {
                loginToServer(SettingsProxy.getLogin(context), SettingsProxy.getPassword(context), cookieJar);
            }
            if (cookieJar.hasToken()) { // already logged in
                JSONObject requestJson = new JSONObject();
                requestJson.put("method", "client_update");
                JSONObject newClient = new JSONObject();
                newClient.put("id", 0);
                newClient.put("name", contactVO.getName());
                newClient.put("surname", contactVO.getSurname());
                newClient.put("patronymic", "");

                JSONArray clientPhones = new JSONArray();
                clientPhones.put(contactVO.getPhone());
                newClient.put("phones", clientPhones);

                requestJson.put("client", newClient);

                RequestBody body = RequestBody.create(JSON, requestJson.toString());
                Request request = new Request.Builder()
                        .url(SERVER_URL).post(body).build();

                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.cookieJar(cookieJar);
                OkHttpClient client = builder.build();

                Response response = null;
                ResponseBody responseBody = null;
                try {
                    response = client.newCall(request).execute();
                    if (response.isSuccessful()) {
                        try {
                            responseBody = response.body();
                            String responseStr = responseBody.string();

                            JSONObject responseJson = new JSONObject(responseStr);
                            contactVO.setFinxId(responseJson.getInt("client_id"));

                            DbHelper dbHelper = new DbHelper(context);
                            if (dbHelper.updatePhoneNumberByPhone(contactVO) <= 0) {
                                successful = false;
                            }
                        } catch (Exception e) {
                            SettingsProxy.setToken(context, "");
                            successful = false;
                        }
                    } else {
                        SettingsProxy.setToken(context, "");
                        successful = false;
                    }
                } finally {
                    if (response != null) {
                        response.close(); // ensure the response (and underlying response body) is closed
                    }
                }
            } else {
                successful = false;
            }
        } catch (Exception e) {
            successful = false;
        }
        if (successful) {
            EventBus.getDefault().post(new RefreshEvent());
        } else {
            EventBus.getDefault().post(R.string.add_person_exception);
        }
    }
}
