package ru.autolainen.phonenumbercheck;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import java.util.Collections;

import static ru.autolainen.phonenumbercheck.ContactVO.HAS_COMMENTS_STATE;
import static ru.autolainen.phonenumbercheck.ContactVO.NO_CLIENT_STATE;
import static ru.autolainen.phonenumbercheck.ContactVO.NO_COMMENTS_STATE;
import static ru.autolainen.phonenumbercheck.ContactVO.UNVERIFIED_STATE;

class CheckPhoneAsyncTask extends BaseCheckPhoneAsyncTask {

    public CheckPhoneAsyncTask(Context context) {
        super(context);
    }

    protected void processResponse(ContactVO contactVO) {
        if (contactVO.getName() == null) {
            new DbHelper(context).insertPhoneNumber(0, contactVO.getPhone(), null,
                    System.currentTimeMillis(), NO_CLIENT_STATE, null);
        } else {
            if (contactVO.getComments().isEmpty()) {
                new DbHelper(context).insertPhoneNumber(contactVO.getFinxId(),
                        contactVO.getPhone(), contactVO.getName() + " " + contactVO.getSurname(),
                        System.currentTimeMillis(), NO_COMMENTS_STATE, null);
            } else {
                long dbId = new DbHelper(context).insertPhoneNumber(contactVO.getFinxId(),
                        contactVO.getPhone(), contactVO.getName() + " " + contactVO.getSurname(),
                        System.currentTimeMillis(), HAS_COMMENTS_STATE, contactVO.getComments());
                if (dbId > 0) {
                    showNotification(context, dbId, contactVO.getPhone(), contactVO.getComments().size());
                }
            }
        }
    }

    @Override
    protected void processFailure(Context context, String phoneNumber) {
        insertUnverifiedPhone(context, phoneNumber);
    }

    private void insertUnverifiedPhone(Context context, String phoneNumber) {
        CommentVO comment = new CommentVO();
        comment.setPublicComment("");
        new DbHelper(context).insertPhoneNumber(0, phoneNumber, "",
                System.currentTimeMillis(), UNVERIFIED_STATE, Collections.singletonList(comment));
    }

    private void showNotification(Context context, long dbId, String phoneNumber, int commentsSize) {
        String contentText = context.getResources().getString(R.string.public_comments) + " " + commentsSize;
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(android.R.drawable.ic_dialog_alert)
                        .setContentTitle(phoneNumber)
                        .setContentText(contentText)
                        .setAutoCancel(true);
        Intent resultIntent = new Intent(context, ContactListActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(ContactListActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify((int) dbId, mBuilder.build());
    }
}
